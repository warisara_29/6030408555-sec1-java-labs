package pilasuk.warisara.lab5;

public class ThailandGoTournament extends Competition{
	
	private String winnerDesc;

	public void Competition(String name, String date, String place) {
	}

	public ThailandGoTournament(String name, String date, String place) {
		super(name, date, place,"");
	}
	/**
	 * @return the winnerDesc
	 */
	public String getWinnerDesc() {
		return winnerDesc;
	}
	/**
	 * @param winnerDesc the winnerDesc to set
	 */
	public void setWinnerDesc(String winnerDesc) {
		this.winnerDesc = winnerDesc;
	}
	/**
	 * override abtract method to show description and rules of THAILAND OPEN GO TOURNAMENT
	 */
	@Override
	void setDescriptionAndRules() {
		System.out.println("=============== Begin: Description and Rules =====================");
		System.out.println("The competition is open for all students and people");
		System.out.println("The competition is devided into four categories");
		System.out.println("1. High Dan (3 Dan and above)");
		System.out.println("2. Low Dan (1-2 Dan)");
		System.out.println("3. High Kyu (1-4 Kyu)");
		System.out.println("4. Low Kyu (5-8 Kyu)");
		System.out.println("=============== End: Description and Rules =====================");
		
	}
	@Override
	public String toString() {
		return super.getName() + " was held on " + super.getDate() + " in " + super.getPlace() + " "
				+ "\nSome winners are " + getWinnerDesc();
	}
}

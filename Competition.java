package pilasuk.warisara.lab5;
/**
 * this program is a abstract class which has four protected variables as name, date, place and description
 * 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   11/02/2018
 **/

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Competition {
	/**
	 * Add protected variables 
	 * implement getter and setter method for four variable
	 * @param name this is the first parameter for get and set name
	 * @param date this is the second parameter for get and set date
	 * @param place this is the third parameter for get and set place
	 * @param description this is the fourth parameter for get and set description
	 * @return name(String), date(LocalDate), place(String), description(String)
	 */
	protected String name;
	protected LocalDate date;
	protected String place;
	protected String description;
	
	public Competition(String name, String date, String place, String description) {
			this.name = name;
			this.date = LocalDate.parse(date, formatter);
			this.place = place;
			this.description = description;
	}
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * this is an abstract method name setDescription
	 * @return nothing
	 */
	abstract void setDescriptionAndRules(); {
	}
	
	public String toString() {
		return name + "(" + description +")" + date + " " + place;
	}
	
	
}

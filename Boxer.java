package pilasuk.warisara.lab5;

/**
 *this program is a subclass of Athlete and has private instance variables sport (that is set to "Boxing"), division and golveSize(size can be a letter Y, S, M, L and XL)
 *
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 * 
 **/

public class Boxer extends Athlete{
	
	/**
	 * Add private instance variables 
	 * static String variable called sport that is set to "Boxing".
	 * private double variable called division.
	 * private int variable called gloveSize.
	 * @param Sport this is the first parameter for set sport to Boxing
	 * @param division this is the second parameter for get and set division
	 * @param gloveSize this is the third parameter for get and set glove size
	 * @return Boxing, division(String), gloveSize(String)
	 */
	
	private static String Sport = "Boxing";
	private String division;
	private String gloveSize;

	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String division, String gloveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		this.division = division;
		this.gloveSize = gloveSize;
	}
	/**
	 * method get is for get boxer's informations from the program
	 * method set is for set boxer's informations from the program
	 */

	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		Boxer.Sport = sport;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGloveSize() {
		return gloveSize;
	}

	public void setGloveSize(String gloveSize) {
		this.gloveSize = gloveSize;
	}
	
	public void play() {
		System.out.println(super.getName() + " throws a punch.");
	}
	
	public void move() {
		System.out.println(super.getName() + " moves around a boxing ring.");
	}
	
	@Override
	public void playSport() {
		System.out.println(super.getName() + " is good at " + getSport());
	}
	/**
	 * this is override method for set the form of the program to show boxer player's  informations
	 * @return getName, getWeight, getHeight, getGender, getNationality, getBirthdate, getSport, getDivision, getGlovesize
	 */
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getDivision() + ", " + getGloveSize() ;
	}
}

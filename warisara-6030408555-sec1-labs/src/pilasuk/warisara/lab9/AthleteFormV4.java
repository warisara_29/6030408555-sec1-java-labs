package pilasuk.warisara.lab9;

/**
 * this program names AthleteFormV4
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   1/04/2018
 **/

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;
import javax.swing.*;
import pilasuk.warisara.lab7.AthleteFromV3;

/**
 * class extends AthleteFromV3 and implements ActionListener
 */
public class AthleteFormV4 extends AthleteFromV3 implements ActionListener{

	public AthleteFormV4(String title) {
		super(title);

	}
	
	private static final long serialVersionUID = 1L;
	
	protected String gender, infomation, changedGender;
	protected JMenuItem blue, green, red, custumcolor, sixteen, twenty, twentyfour, custumsize;
	
	/**
	 * method actionPerformed is for check when user click buttons
	 */
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == ok_Button) {
			handleOKButton();
		} 
		else if (src == cancel_Button) {
			handleCancelButton();
		}
		else if (src == athlete) {
			JComboBox<?> athlete = (JComboBox<?>) event.getSource();
			Object selectedtype = athlete.getSelectedItem();
			JOptionPane.showMessageDialog(null, "Your athlete type is changed to " + selectedtype);
		}

		else if ((src == radioButton1) || (src == radioButton2)) {
			changedGender();
		}
	}

	/**
	 * method addListeners is for listen if user click buttons
	 */
	protected void addListeners() {
		ok_Button.addActionListener(this);
		cancel_Button.addActionListener(this);
		radioButton1.addActionListener(this);
		radioButton2.addActionListener(this);
		athlete.addActionListener(this);
	}
	
	/**
	 * method handleOKButton is for check when user click ok button. 
	 * it will show athlete form with name, birthdate, weight, height, nationality, gender, competition and athlete type 
	 */
	protected void handleOKButton() {
		/*
		 * if condition is for check if user click radiobutton1 or radiobutton2
		 * if user click radiobutton1 gender will show as male
		 * if user click radiobutton2 gender will show as female
		 */
		if (radioButton1.isSelected()) {
			gender = "male";
		}
		else if (radioButton2.isSelected()) {
			gender = "female";
		}
		
		infomation = "Name = " + nameField.getText() + ", Birthdate = " + birthdateField.getText() + 
				", Weight = " + weightField.getText() + ", Height = " + heightField.getText() +
				", Nationality = " + nationalityField.getText() + 
				"\nGender = " + gender +
				"\nCompetition = " + text_area.getText() +
				"\nType = " + athlete.getSelectedItem();
				JOptionPane.showMessageDialog(null , infomation); //for show message dialog with all in formation
	}
	/**
	 * method handleCancelButton is when user click cancel button
	 * it will delete all text and set athlete type to Boxer 
	 */
	protected void handleCancelButton() {
		nameField.setText("");
		birthdateField.setText("");
		weightField.setText("");
		heightField.setText("");
		nationalityField.setText("");
		radioButton1.setSelected(true);
		text_area.setText("");
		athlete.setSelectedItem("Boxer");
	}
	
	/**
	 * method changedGender is for check when user click radiobutton1 or radiobutton2
	 * and for set message dialog to below main panel
	 */
	protected void changedGender() {
		if (radioButton1.isSelected()) {
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			int w = this.getHeight();
			int h = this.getHeight();
			int x = (dim.width - w)/2 + (w/2 + 10);
			int y = (dim.height - (h/2)) - 20;
			gender = "male";
			changedGender = "Your gender type is now changed to " + gender;
			final JOptionPane pane = new JOptionPane(changedGender);
			final JDialog d = pane.createDialog((JFrame)null, "message");
			d.setLocation(x,y);
			d.setVisible(true);
		}
		else if (radioButton2.isSelected()) {
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			int w = this.getWidth();
			int h = this.getHeight();
			int x = (dim.width - w)/2 + (w/2 + 10);
			int y = (dim.height - h/2) - 20;
			gender = "female";
			changedGender = "Your gender type is now changed to " + gender;
			final JOptionPane pane = new JOptionPane(changedGender);
			final JDialog d = pane.createDialog((JFrame)null, "message");
			d.setLocation(x,y);
			d.setVisible(true);
		}
		
	}
	
	protected void addComponents() {
		super.addComponents();
		super.addMenuBar();
		addListeners();
	}
	
    public static void createAndShowGUI(){
    	AthleteFormV4 AthleteForm4 = new AthleteFormV4("Athlete From V4");
    	AthleteForm4.addComponents();
    	AthleteForm4.setFrameFeatures();
    }
    
	/**
	 * @param args
	 * for run method createAndShowGUI
	 */
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			createAndShowGUI();
    		}
    	});
    }
}

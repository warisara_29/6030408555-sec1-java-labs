package pilasuk.warisara.lab12;

import java.awt.event.ActionEvent;
import java.util.Collections;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import pilasuk.warisara.lab11.AthleteFormV8;

public class AthleteFormV9 extends AthleteFormV8 {
	
	public String searchName;

	public AthleteFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	protected void addAthlete() {
		super.addAthlete();
	}
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == menuItem_Search) {
			searchMenu();
		} else if (src == menuItem_Remove) {
			RemoveMenu();
		}
	}
	
	private void RemoveMenu() {
		// TODO Auto-generated method stub
		
	}

	private void searchMenu() {
		searchName = JOptionPane.showInputDialog("Please enter the athlete name");
		int count = 0;
		for (int i = 0; i < athletes.size(); i++) {
			if ((athletes.get(i).getName()).contains(searchName)) {
				count = 0;
				JOptionPane.showMessageDialog(null, athletes.get(i));
				break;
			}
			else {
				count += 1; 
				if (count == athletes.size()) { 
					JOptionPane.showMessageDialog(null, searchName + " is not found");
				}
			}
		}
	}
	
	@Override
	protected void addMenuBar() {
		super.addMenuBar();
		menuItem_Search.addActionListener(this);
		menuItem_Remove.addActionListener(this);
		
	}

	public static void createAndShowGUI(){
		AthleteFormV9 AthleteForm9 = new AthleteFormV9("Athlete Form V9");
		AthleteForm9.addComponents();
		AthleteForm9.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

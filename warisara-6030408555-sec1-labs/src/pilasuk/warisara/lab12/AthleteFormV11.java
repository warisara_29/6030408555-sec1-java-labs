package pilasuk.warisara.lab12;

import javax.swing.SwingUtilities;

public class AthleteFormV11 extends AthleteFormV10 {

	public AthleteFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public static void createAndShowGUI(){
		AthleteFormV11 AthleteForm11 = new AthleteFormV11("Athlete Form V11");
		AthleteForm11.addComponents();
		AthleteForm11.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

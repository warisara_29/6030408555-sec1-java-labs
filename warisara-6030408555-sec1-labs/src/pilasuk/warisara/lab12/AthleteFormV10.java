package pilasuk.warisara.lab12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;

import pilasuk.warisara.lab5.Athlete;

public class AthleteFormV10 extends AthleteFormV9 {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected ArrayList<Athlete> athleteList;
	protected FileWriter fileWriter;
	protected JFileChooser fileChooser;
	protected File selectedFile;
	protected FileInputStream fileInput;

	public AthleteFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void saveFile() {
		System.out.println("save");
		fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		int returnValue = fileChooser.showSaveDialog(this);
		if (returnValue == JFileChooser.APPROVE_OPTION) { // Action when user choose file
			selectedFile = fileChooser.getSelectedFile();
			System.out.println(selectedFile.getName());
			try {
				FileOutputStream fileOutput = new FileOutputStream(selectedFile);
				ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
				objectOutput.writeObject(athleteV8);
				objectOutput.close();
				fileOutput.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (returnValue == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void openFile() {
		JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		int returnValue = fileChooser.showOpenDialog(this);

		if (returnValue == JFileChooser.APPROVE_OPTION) { // Action when user choose file
			selectedFile = fileChooser.getSelectedFile();
			System.out.println(selectedFile.getName());
			try {
				fileInput = new FileInputStream(selectedFile);
				ObjectInputStream objectInput = new ObjectInputStream(fileInput);
				athleteList = (ArrayList<Athlete>) objectInput.readObject();
				for(int i=0; i<athleteList.size(); i++) {
					athletes.add(athleteList.get(i));
				}
				objectInput.close();
				fileInput.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (returnValue == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this, "Open command cancelled by user.");
		}

	}

	
	public static void createAndShowGUI(){
		AthleteFormV10 AthleteForm10 = new AthleteFormV10("Athlete Form V10");
		AthleteForm10.addComponents();
		AthleteForm10.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

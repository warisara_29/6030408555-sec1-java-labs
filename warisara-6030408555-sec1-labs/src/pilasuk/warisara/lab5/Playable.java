package pilasuk.warisara.lab5;

/**
 * this is interface class name Playable. The interface contains one method called play().
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/
public interface Playable {
	public void play();
}
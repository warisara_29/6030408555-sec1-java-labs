package pilasuk.warisara.lab5;

/**
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/
public class InterestingCompetitions {
	public static void main(String[] args) {
		SuperBowl superBowlLII = new SuperBowl("SuperBowlLII", "04/02/2018", "Minnesota, United States", 
				"the 52nd Super Bowl", " New England Patriots", "Philadelphia Eagles", "Philadelphia Eagles");
		superBowlLII.setDescriptionAndRules();
		System.out.println(superBowlLII);
		SuperBowl superBowlLI = new SuperBowl("SuperBowlLI", "05/02/2017", "Texas, United States", 
				"the 51st Super Bowl", " New England Patriots", "Philadelphia Eagles", "New England Patriots");
		System.out.println(superBowlLI);
		ThailandGoTournament the20thGoOpen = new ThailandGoTournament("THE 20th THAILAND OPEN GO TOURNAMENT", 
				"20/08/2017", "Samut Prakan, Thailand");
		the20thGoOpen.setWinnerDesc("»ÃÐàÀ· High kyu ÃÒ§ÇÑÅÃÍ§ª¹ÐàÅÔÈÍÑ¹´Ñº 2 ä´éá¡è  àÁ¸ÑÊ  á¡éÇ·ÃÒÂ¢ÒÇ");
		the20thGoOpen.setDescriptionAndRules();
		System.out.println(the20thGoOpen);	
	}

}
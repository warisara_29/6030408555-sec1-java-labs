package pilasuk.warisara.lab5;

/**
 * this program is enum method that contains two values: MALE and FEMALE to be used as a value of the variable gender
 * 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   04/02/2018
 * 
 **/

public enum Gender {
	MALE , FEMALE ;
}

package pilasuk.warisara.lab5;

/**
 * this program is to get the super bowl informations and for show description and rules about super bowl.
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/

public class SuperBowl extends Competition{
	
	/**
	 * this is a subclass of Competition and has three additional variables
	 * as AFCTeam, NFCTeam, and winningTeam
	 * it has two constructors.
	 * the first accepts variables name, date, place, and description
	 * the second accepts all variables 
	 * @param AFCTeam is the first parameter to get and set AFC team
	 * @param NFCTeam is the second parameter to get and set NFC team
	 * @param winningTeam is the third parameter to get and set winning team
	 * @return AFCTeam(String), NFCTeam(String), winningTeam(String)
	 */

	private String AFCTeam;
	private String NFCTeam;
	private String winningTeam;

	public void Competition(String name, String date, String place, String description) {
	}

	public SuperBowl(String name, String date, String place, String description, String AFCTeam, String NFCTeam, String winningTeam) {
		super(name, date, place, description);
		this.AFCTeam = AFCTeam;
		this.NFCTeam = NFCTeam;
		this.winningTeam = winningTeam;
	}

	/**
	 * @return the aFCTeam
	 */
	public String getAFCTeam() {
		return AFCTeam;
	}

	/**
	 * @param aFCTeam the aFCTeam to set
	 */
	public void setAFCTeam(String aFCTeam) {
		AFCTeam = aFCTeam;
	}

	/**
	 * @return the nFCTeam
	 */
	public String getNFCTeam() {
		return NFCTeam;
	}

	/**
	 * @param nFCTeam the nFCTeam to set
	 */
	public void setNFCTeam(String nFCTeam) {
		NFCTeam = nFCTeam;
	}

	/**
	 * @return the winningTeam
	 */
	public String getWinningTeam() {
		return winningTeam;
	}

	/**
	 * @param winningTeam the winningTeam to set
	 */
	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}
	/**
	 * override abtract method to show description and rules of super bowl
	 */
	@Override
	void setDescriptionAndRules() {
		System.out.println("=============== Begin: Description and Rules =====================");
		System.out.println("SuperBowLII is played between the champions of Nation Football Conference (NFC) and the American Football Conference (AFC).");
		System.out.println("The game plays in four quarters while each quarter takes about 15 minutes.");
		System.out.println("=============== End: Description and Rules =====================");
	}
	
	@Override
	public String toString() {
		return super.getName() + "(" + super.getDescription() + ") was held on " + super.getDate() + " in " + super.getPlace() + ". \nIt was the game between " 
				+ getAFCTeam() + " vs. " + getNFCTeam() + ". \nThe winner was " + getWinningTeam();
	}
}

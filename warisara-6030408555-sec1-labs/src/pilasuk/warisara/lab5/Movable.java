package pilasuk.warisara.lab5;

/**
 * this is interface class name Movable. The interface contains one method called move().
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/
public interface Movable {
	public void move();
}
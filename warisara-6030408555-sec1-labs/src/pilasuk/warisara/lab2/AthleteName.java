package pilasuk.warisara.lab2;

public class AthleteName {
	public static void main(String[] args) {
		if ( args.length == 3 ) {
			String athletename = args[0]; //arg1
			String sportname = args[1]; //arg2
			String athlete_nationality = args[2]; //arg3
			System.out.println("My favorite athlete is " + athletename + " who plays " + sportname + " and has nationality as " + athlete_nationality );
		}
		else {
			System.err.println("Athlete <athlete name> <sport name> <nationality>");
		}
	}
}



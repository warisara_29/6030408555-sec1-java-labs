package pilasuk.warisara.lab2;

public class SubstringChecker {
	public static void main(String[] args) {
		if (args.length  < 2) {
			System.err.println("SubstringChecker <substring> <str1> ...");
			System.exit(0);
		}
		String sub_string = args[0];
		int number = 0; //for count number of strings
		for(int i = 1; i < args.length; i++ ) { //get input unlimited arguments
			if (args[i].indexOf(sub_string) != -1) { //find argument that contains substring
				number += 1;
				System.out.println("String " + i + " : " + args[i] + " contains " + sub_string);
			} else {
				System.out.println("There are no string that contains " + sub_string);
				System.exit(0);
			}
		}
		if (number <= 1) {
			System.out.println("There is " + number + " string that contains " + sub_string);
			System.exit(0);
		}
		System.out.println("There are " + number + " string that contains " + sub_string);
	}
}

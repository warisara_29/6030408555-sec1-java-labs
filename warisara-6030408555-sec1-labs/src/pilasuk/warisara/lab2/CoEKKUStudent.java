package pilasuk.warisara.lab2;

public class CoEKKUStudent {
	public static void main(String[] args) {
		if ( args.length == 5 ) {
			String name = args[0]; //get student's name
			String id = args[1]; //get student's ID
			String gpa = args[2]; //get student's GPA
			String academic_year = args[3]; //get student's academic year
			String parents_income = args[4]; //get student's parent income
			double new_id  = Double.parseDouble(args[1]); //convert string value to a double value
			double FIRST_ID  = Double.parseDouble("3415717"); //add first CoE student's in a double value
			double id_first = new_id - FIRST_ID; //find a different ID value from the first CoE KKU student's ID in a double value
			long different_id = (long) id_first; //convert double value to a long value
			System.out.println( name + " has ID = " + id + " GPA = " + gpa + " year = " + academic_year + " parent's income = " + parents_income );
			System.out.println( "Your ID is different from the first CoE student's ID by " + different_id );	
		}
		else {
			System.err.println("CoEKKUStudent <name> <ID> <GPA> <academic year> <parent's income>");			
		}		
	}
}

package pilasuk.warisara.lab2;

public class Calculator {
	public static void main(String[] args) {
		if (args.length <= 2) {
			System.err.println("Calculator <operend1> <operend2> <oprend3>");
			System.exit(1);
		}
		float result = 0;
		String operend3 = args[2];
		float operend1 = Float.parseFloat(args[0]);
		float operend2 = Float.parseFloat(args[1]);
		if (operend3.equals("+")) {
			result = operend1 + operend2;	
		} else if (operend3.equals("-")) {
			result = operend1 - operend2;
		} else if (operend3.equals(" * ") || operend3.equals(".classpath")) {
			operend3 = "*";
			result = operend1 * operend2;
		} else if (operend3.equals("/")) {
			if (operend2 != 0) {
				result = (operend1 / operend2);
			} else {
				System.out.println("The second operend cannot devide by zero.");
				System.exit(0);;
			}	
		}
		System.out.printf(operend1 + " " + operend3 + " " + operend2 + " = " + "%.2f",result);
	}
}

package pilasuk.warisara.lab4;

import java.time.temporal.ChronoUnit;

public class BadmintonPlayer extends Athlete {

	private static String Sport = "BAdminton";
	private double racketLenght;
	private int worldRanking;

	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality, String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLenght = racketLength;
		this.worldRanking = worldRanking;
	}
	//method get is for get badminton player's informations from the program
	//method set is for set badminton player's informations from the program
	public double getRacketLenght() {
		return racketLenght;
	}

	public void setRacketLenght(double racketLenght) {
		this.racketLenght = racketLenght;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	

	public static String getSport() {
		return Sport;
	}

	public void setSport(String sport) {
		BadmintonPlayer.Sport = sport;
	}
	//this is override method for set the form of the program to show badminton player's  informations
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getRacketLenght() + "cm, rank: " + getWorldRanking() ;
	}
	
	//this method is for compare athlete age by get athlete's birthdate and import java.lang ChronoUnit  
	public void compareAge(Object localDate) {
		int year = (int) ChronoUnit.YEARS.between(super.getBirthdate(), (((Athlete) localDate).getBirthdate()));
		if (year < 0) {
			System.out.println(((Athlete) localDate).getName() + " is " + Math.abs(year) + " years older than " + super.getName());
		} else if (year == 0) {
			System.out.println(((Athlete) localDate).getName() + " had year old equal to " + super.getName());
		} else {
			System.out.println(super.getName() + " is " + year + " years older than " + ((Athlete) localDate).getName());
		}
	}
}
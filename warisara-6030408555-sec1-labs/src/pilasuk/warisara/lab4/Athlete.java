package pilasuk.warisara.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Athlete {
	
	// this method is for set variables to record the Athlete information and it will send data to the private instance variable

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate , formatter);
	}
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); //for set date(day/month/year) by import java.lang LocalDate
	
	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	
	//method get is for get athlete information from program
	//method set is for set athlete information from program
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getWeight() {
		return weight;		
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	//this is override method for set the from of the program to show athlete informations
	@Override
	public String toString() {
		return "Athlete [" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", " + birthdate + " ]";
	}	
}


package pilasuk.warisara.lab4;

public class Boxer extends Athlete{
	private static String Sport = "Boxing";
	private String division;
	private String gloveSize;

	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String division, String gloveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		this.division = division;
		this.gloveSize = gloveSize;
	}
	
	//method get is for get boxer's informations from the program
	//method set is for set boxer's informations from the program

	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		Boxer.Sport = sport;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGloveSize() {
		return gloveSize;
	}

	public void setGloveSize(String gloveSize) {
		this.gloveSize = gloveSize;
	}
	//this is override method for set the form of the program to show badminton player's  informations
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getDivision() + ", " + getGloveSize() ;
	}
}

package pilasuk.warisara.lab4;

public class Footballer extends Athlete {
	
	private static String Sport = "American Football";
	private String position;
	private String team;

	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String position, String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	}
	
	//method get is for get football player's informations from the program
	//method set is for set football player's informations from the program
	
	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		Footballer.Sport = sport;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	//this is override method for set the form of the program to show football player's  informations
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getPosition() + ", " + getTeam() ;
	}
	
}

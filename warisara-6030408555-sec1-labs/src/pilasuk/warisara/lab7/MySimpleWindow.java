
package pilasuk.warisara.lab7;

/**
 * this program names MySimple window that can show 2 buttons, OK button and Cancel button
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   19/02/2018
 **/

import java.awt.*;

import javax.swing.*;

class MySimpleWindow extends JFrame {
	/**
	 * MySimpleWindow extends from class JFrame
	 * MySimpleWindow has 1 constructor and 4 methods
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JButton cancel_Button, ok_Button;
	protected JPanel panel_Panel;
	
	public MySimpleWindow(String title) { //constructor
		super (title);
	}
	
	/**
	 * method addComponents is for set panel(JPanel), set buttons(OK,Cancel)
	 * and set border layout = south
	 */
	protected void addComponents() {
		panel_Panel = new JPanel();
		setLayout(new BorderLayout());
		
		JPanel panel = new JPanel();
		ok_Button = new JButton("OK");
		cancel_Button = new JButton("Cancel");
		panel.add(ok_Button);
		panel.add(cancel_Button);
		
		add(panel, BorderLayout.SOUTH);
	}
	
	/**
	 * method setFrameFeatures is for set frame to be center 
	 * can exit when user initiates a "close" on this frame
	 */
	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
		setVisible(true);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	}
	/**
	 * method createAndShowGUI is for create and show method addCompoents and method setFrameFeatures
	 * set mySimpleWindow title = My Simple Window
	 */
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
		
	/**
	 * @param args
	 * for run method createAndShowGUI
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
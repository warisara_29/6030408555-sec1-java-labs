package pilasuk.warisara.lab7;

/**
 * this program names AthleteFromV2
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   12/03/2018
 **/

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class AthleteFromV2 extends AthleteFromV1 {

	public AthleteFromV2(String title) {
		super(title);
	}

	/**
	 * MySimpleWindow extends from class MySimpleWindow
	 * MySimpleWindow has 1 constructor and 3 methods
	 * 
	 */
	private static final long serialVersionUID = 3L;

	/**
	 * method addMenuBar is for set label names Gender and create JRadioButton(Male,Female)
	 */
	protected void addComponents() {
		super.addComponents();
		JPanel labelNration = new JPanel();
		JPanel ration = new JPanel();

		JLabel labelShow = new JLabel("Gender: ");
		labelNration.setLayout(new GridLayout(1, 2));
		labelNration.add(labelShow);
		labelNration.add(ration);
		panel_Panel.add(labelNration);

		JRadioButton radioButton1 = new JRadioButton("Male", true);
		JRadioButton radioButton2 = new JRadioButton("Female");
		ration.setLayout(new FlowLayout());
		ration.add(radioButton1);
		ration.add(radioButton2);

		ButtonGroup group = new ButtonGroup();
		group.add(radioButton1);
		group.add(radioButton2);
		
		labelNration.add(ration);
		

		JPanel textpanel = new JPanel();
		textpanel.setLayout(new GridLayout(2, 1));
		textpanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0)); 
		JLabel competitionLabel = new JLabel("Competition: ");
		JTextArea text_area = new JTextArea(2, 35);
		text_area.setText("Competed in the 31st national championship");
		textpanel.add(competitionLabel);
		textpanel.add(text_area);
		panel_Panel.add(textpanel);
		
		add(panel_Panel, BorderLayout.NORTH);
	}
	/**
	 * method createAndShowGUI is for create and show method addCompoents and method setFrameFeatures
	 * set AthleteFromV2 title = Athlete From V2
	 */
    public static void createAndShowGUI(){
    	AthleteFromV2 athleteFrom2 = new AthleteFromV2("Athlete From V2");
    	athleteFrom2.addComponents();
    	athleteFrom2.setFrameFeatures();
    }
    
	/**
	 * @param args
	 * for run method createAndShowGUI
	 */

    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			createAndShowGUI();
    		}
    	});
    }

}

package pilasuk.warisara.lab10;

/**
 *  
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   6/04/2018
 * 
 **/

import javax.swing.SwingUtilities;

import pilasuk.warisara.lab10.SimpleGameWindow;


public class PongGameV3 extends SimpleGameWindow{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV3(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV3 window = new PongGameV3("CoE Pong Game V3");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanelV2());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
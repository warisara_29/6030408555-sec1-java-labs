package pilasuk.warisara.lab10;

/**
 *  
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   6/04/2018
 * 
 **/

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

public class MovingPongGamePanelV2 extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;
	protected MovingBall movingBall;
	protected Thread running;
	protected Random rand;
	protected int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;
	protected int velX = 1, velY = 1;

	public MovingPongGamePanelV2() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);
		
		// initialize the pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2 - 30, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH - 25,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2 - 30, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		
		//resetBall();
		movingBall = new MovingBall((SimpleGameWindow.WIDTH)/2- ballR - 15, (SimpleGameWindow.HEIGHT)/2 - ballR - 30, ballR, 2, 2);
		
		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH - 25, SimpleGameWindow.HEIGHT - 60);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	@Override
	public void run() {

		while (true) {

			moveBall();
			
			repaint();
			this.getToolkit().sync(); // to flush the graphic buffer

			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	// update position of the ball
	private void moveBall() {
		
		movingBall.x = movingBall.x + velX; 
		movingBall.y = movingBall.y + velY;
		
		rand = new Random();
		//velX = rand.nextInt(1);
		//velY = rand.nextInt(1);
		if (movingBall.x - (2 * ballR) < - 25) {
			if (movingBall.intersects(movableLeftPad)) {
				velX = (rand.nextInt(2));
				movingBall.x = movingBall.x + velX;
				movingBall.y = movingBall.y + velY;
			}else {
				movingBall.x = movingBall.x + velX;
				movingBall.y = movingBall.y + velY;
			}
		} else if (movingBall.x - (2 * ballR) > SimpleGameWindow.WIDTH - 150) {
			if (movingBall.intersects(movableRightPad)) {
				velX = (-rand.nextInt(2));
				movingBall.x = movingBall.x + velX;
				movingBall.y = movingBall.y + velY;
			}else {
				movingBall.x = movingBall.x + velX;
				movingBall.y = movingBall.y + velY;
			}
		}
		if (movingBall.y - (2 * ballR) < - 45) {
			velY = (rand.nextInt(2));
			movingBall.x = movingBall.x + velX;
			movingBall.y = movingBall.y + velY;
		} else  if (movingBall.y - (2 * ballR) > SimpleGameWindow.HEIGHT - 140) {
			velY = (-rand.nextInt(2));
			movingBall.x = movingBall.x + velX;
			movingBall.y = movingBall.y + velY;
		}
	
	}


	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2 - 15, 0, SimpleGameWindow.WIDTH / 2 - 15, SimpleGameWindow.HEIGHT - 60);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT - 60);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW() - 25, 0, SimpleGameWindow.WIDTH - movableRightPad.getW() - 25,
				SimpleGameWindow.HEIGHT - 60);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4 - 25, SimpleGameWindow.HEIGHT / 5);

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		}
		if (key == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		}
		if (key == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		}
		if (key == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
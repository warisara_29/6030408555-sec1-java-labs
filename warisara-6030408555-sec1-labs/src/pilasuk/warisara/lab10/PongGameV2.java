package pilasuk.warisara.lab10;

/**
 *  
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   6/04/2018
 * 
 **/

import javax.swing.SwingUtilities;

import pilasuk.warisara.lab10.SimpleGameWindow;

public class PongGameV2 extends SimpleGameWindow{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV2(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV2 window = new PongGameV2("CoE Pong Game V2");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanel());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

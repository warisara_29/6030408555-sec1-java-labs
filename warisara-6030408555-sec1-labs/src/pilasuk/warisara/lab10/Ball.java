package pilasuk.warisara.lab10;

/**
 *  
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   6/04/2018
 * 
 **/

import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 150974525667001681L;
	
	private int r;
	
	public Ball(int x, int y, int r) {
		super(x, y, 2*r, 2*r);
		this.r = r;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
}

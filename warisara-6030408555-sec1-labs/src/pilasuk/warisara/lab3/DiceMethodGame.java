package pilasuk.warisara.lab3;

import java.util.Scanner;
import java.util.Random;

public class DiceMethodGame {
	
	static int humanGuest;
	static int computerScore;
	public static void main(String[] args) {
		humanGuest =  acceptInput();
		computerScore = genDiceroll();
		displayWinner(humanGuest,computerScore);
		
	}
	static int acceptInput() { //reads in a number to be the guest from a human
		Scanner sc_number = new Scanner(System.in); //scan number from user
		System.out.print("Enter your guest (1-6) : ");
		int acceptInput = sc_number.nextInt();
		sc_number.close();
		if ((acceptInput < 1) || (acceptInput > 6)) { //check incorrect number
			System.err.println("Incorrect number. Only 1-6 can be entered");
			System.exit(0);
		} else {
			System.out.println("You have guested number : " + acceptInput);
		}
		return acceptInput; //return result
	}
	static int genDiceroll() { //generate a number between 1 to 6 to simulates a dice rolls
		Random number_random = new Random(); //random number from user
		int genDiceroll = number_random.nextInt(6) + 1;
		System.out.println("Computer has rolled number : " + genDiceroll);
		return genDiceroll; //return result
	}
	static void displayWinner(int humanGuest, int computerScore) { //accepts humanGuest and computerScore 
		int diff_number = Math.abs(computerScore - humanGuest); //find the winner
		if ((diff_number <= 1) || (diff_number == 5)) {
			System.out.println("You win.");
		} else {
			System.out.println("Computer wins."); 
		}
	}	
}
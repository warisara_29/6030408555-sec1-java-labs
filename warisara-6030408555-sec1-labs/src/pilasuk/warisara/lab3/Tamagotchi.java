package pilasuk.warisara.lab3;

import java.util.Scanner;

public class Tamagotchi {
	public static void main(String[] args) {
		Scanner user_scanner = new Scanner(System.in); //scan 3 integers from user
		System.out.print("Enter your Tamagochi info : ");
		int live = Integer.parseInt(user_scanner.next()); //represents how many hours your tamagotchi pet will live
		int fed = Integer.parseInt(user_scanner.next()); //a time interval when you have to feed your pet
		int watered = Integer.parseInt(user_scanner.next()); //a time interval when you have to water your pet
		user_scanner.close();
		System.out.println("Your Tamagotchi will live for " + live + " hours");
		System.out.println("It needs to be fed every " + fed + " hours");
		System.out.println("It needs to be watered every " + watered + " hours");
		System.out.println(" ");
		
		int water_feed = 0; //count the time that we can fed and watered pet at the same time
		for (int i  = live; i > 0; i--) {
			int devide_fed = i % fed;
			int devide_watered = i % watered;
			if (devide_fed == 0 && devide_watered == 0) {
				water_feed += 1;
			}
		}
	
		int water_time = (live / watered) - water_feed; //find result only water time
		int feed_time = (live / fed) - water_feed; //find result only feed time
		
		System.out.println("You need to water-feed : " + water_feed + " times");
		System.out.println("You need to water : " + water_time + " times");
		System.out.println("You need to feed : " + feed_time + " times");
	}
}

package pilasuk.warisara.lab3;

import java.util.Scanner;
import java.util.Random;

public class DiceGame {
	public static void main(String[] args) {
		Scanner sc_number = new Scanner(System.in); //scan number from user
		System.out.print("Enter your guest (1-6) : ");
		int guested_number = sc_number.nextInt();
		sc_number.close();
		Random number_random = new Random(); //random number from computer
		int rolled_number = number_random.nextInt(6) + 1;
		
		if ((guested_number < 1) || (guested_number > 6)) { //check if number incorrect
			System.err.println("Incorrect number. Only 1-6 can be entered");
			System.exit(0);
		} else {
			System.out.println("You have guested number : " + guested_number);
			System.out.println("Computer has rolled number : " + rolled_number);
		}
		int diff_number = Math.abs(rolled_number - guested_number);  //find the winner
		if ((diff_number <= 1) || (diff_number == 5)) { 
			System.out.println("You win.");
		} else {
			System.out.println("Computer wins.");
		}
	}
}
package pilasuk.warisara.lab6;
/**
 * The Author program implements an application that record Author personal information : name, email, gender
 *
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   16/02/2018
 * 
 **/

public class Author {

	/**
	   * The class has private instance variables to record 
	   * the following information: name, email, gender
	   * @param name this is the first parameter to get and set name 
	   * @param email this is the second parameter to get and set email
	   * @param gender this is the third parameter to get and set gender (male and female)
	   * @return name(string), email(String), gender(char)
	   **/

	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		this.name = name;
		this.email = email;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}
	/**
	 * this is override method for set the form of the program to show author informations
	 * @return name, email, gender
	 */
	@Override
	public String toString() {
		return "Author [ name = " + getName() + ", email = " + getEmail() + ", gender = " + getGender() + " ]";
		
	}
}

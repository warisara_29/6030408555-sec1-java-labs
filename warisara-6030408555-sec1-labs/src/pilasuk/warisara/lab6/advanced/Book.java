package pilasuk.warisara.lab6.advanced;

import java.util.Arrays;

import pilasuk.warisara.lab6.Author;

public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qyt = 0;
	
	public Book(String name, Author[] authors, double price) {
		this.name = name;
		this.price = price;
		this.authors = authors;
	}
	
	public Book(String name, Author[] authors, double price, int qyt) {
		this.name = name;
		this.price = price;
		this.qyt = qyt;
		this.authors = authors;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author[] getAuthors() {
		return authors;
	}

	public void setAuthors(Author[] authors) {
		this.authors = authors;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQyt() {
		return qyt;
	}

	public void setQyt(int qyt) {
		this.qyt = qyt;
	}
	
	public String toString() {
		return "Book [name = " + name + ", authors = {" + Arrays.toString(authors) + "}, \nprice = " + price + ", qyt = " + qyt ;

	}
	
	public String getAuthorNames() {
		return authors[0].getName() + ", " + authors[1].getName();
	}
}

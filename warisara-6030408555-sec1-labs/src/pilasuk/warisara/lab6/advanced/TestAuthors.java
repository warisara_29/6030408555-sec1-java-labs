package pilasuk.warisara.lab6.advanced;

import pilasuk.warisara.lab6.Author;

public class TestAuthors {
	public static void main(String[] args) {
		// Declare and allocate an array of Authors
		Author[] authors = new Author[2];
		authors[0] = new Author("James Gosling", "james@somewhere.com", 'm');
		authors[1] = new Author("Ken Arnold", "ken@nowhere.com", 'm');
		System.out.println(authors);

		// Declare and allocate a Book instance
		Book java = new Book("The Java programming language", authors, 19.99, 99);
		System.out.println(java);  // toString()
                        System.out.println("=== Author names of this book are ===");
		System.out.println(java.getAuthorNames());
	}
}


package pilasuk.warisara.lab6;

import pilasuk.warisara.lab5.Gender;

public class ThaiBadmintonPlayer extends pilasuk.warisara.lab5.BadmintonPlayer{

	private String equipment = "ลูกขนไก่";
	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, "Thai",birthdate, racketLength, worldRanking);
	}
	/**
	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	**/
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getRacketLenght() + "cm, rank: " + getWorldRanking() + " equipment:" + equipment;
	}
	public void play() {
		System.out.println(getName() + " hits " + equipment); 
	}
}

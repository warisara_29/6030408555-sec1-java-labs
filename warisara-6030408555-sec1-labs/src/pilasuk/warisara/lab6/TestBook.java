package pilasuk.warisara.lab6;

public class TestBook {

	public static void main(String[] args) {
		// Construct an author instance
		Author win = new Author("ÇÔ¹·Ãì àÅÕÂÇÇÒÃÔ³", "win@winbookclub.com", 'm'); // Test the constructor
		System.out.println(win);  // Test toString()

		Book democracyBook = new Book("»ÃÐªÒ¸Ô»äµÂº¹àÊé¹¢¹Ò¹", win, 19.95, 99);  // Test Book's Constructor
		System.out.println(democracyBook);  // Test Book's toString()
		 
		// Test Getters and Setters
		democracyBook.setPrice(29.95);
		democracyBook.setQty(28);
		System.out.println("name is: " + democracyBook.getName());
		System.out.println("price is: " + democracyBook.getPrice());
		System.out.println("qty is: " + democracyBook.getQty());
		System.out.println("Author is: " + democracyBook.getAuthor());  // Author's toString()
		System.out.println("Author's name is: " + democracyBook.getAuthor().getName());
		System.out.println("Author's email is: " + democracyBook.getAuthor().getEmail());

		// Use an anonymous instance of Author to construct a Book instance
		Book anotherBook = new Book("«ÍÂà´ÕÂÇ¡Ñ¹", 
		      new Author("ÇÒ³Ôª ¨ÃØ§¡Ô¨Í¹Ñ¹µì", "wanich@gmail.com", 'm'), 29.95);
		System.out.println(anotherBook);  // toString()

	}
}
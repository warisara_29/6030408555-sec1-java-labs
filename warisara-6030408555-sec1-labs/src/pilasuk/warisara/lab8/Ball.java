package pilasuk.warisara.lab8;

/**
 * this program names Ball 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double {

	/**
	 * programe extends from Ellipse2D for draw the ball 
	 * x is width
	 * y is height
	 * r is radius 
	 */
	
	private static final long serialVersionUID = 1L;
	
	protected int r;

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
	public Ball(int x, int y, int r) {
		super (x,y,2*r,2*r);
		this.r = r;
	}
	
}

package pilasuk.warisara.lab8;

/**
 * this program names BouncyBall
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class BouncyBall extends JPanel implements Runnable {

	/**
	 * program extends JPanel
	 * program implements class Runnable
	 */
	private static final long serialVersionUID = 1L;
	
	MovingBall ball;
	Thread running;
	Rectangle2D.Double box;
	int velX = 2;
	int velY = 2;
	
	public BouncyBall() {
		/** 
		 * this method is for set background color is green
		 * set box position 
		 * set ball position in the middle
		 */
		setBackground(Color.GREEN);
		box = new Rectangle2D.Double(0,0,SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);
		ball = new MovingBall((SimpleGameWindow.WIDTH)/2-20, (SimpleGameWindow.HEIGHT)/2-20, 20, 2, 2);
		running = new Thread(this);
		running.start();
	}
	
	/**
	 * Override method run is for make balls run by check if else
	 */
	@Override
	public void run() {
		
		while(true) {
			
			ball.x = ball.x + velX;
			ball.y = ball.y + velY;
			
			if (ball.x - (2*20) < SimpleGameWindow.WIDTH-695) {
				velX = velX*(-1);
				ball.y = ball.y + velY;
				ball.x = ball.x + velX;
			} else if (ball.x + (2*20) > SimpleGameWindow.WIDTH + 20) {
				velX = velX*(-1);
				ball.y = ball.y + velY;
				ball.x = ball.x + velX;
			}
			if (ball.y - (2*20) < SimpleGameWindow.HEIGHT-555) {
				velY = velY*(-1);
				ball.y = ball.y + velY;
				ball.x = ball.x + velX;
			} else if (ball.y + (2*20) > SimpleGameWindow.HEIGHT + 8) {
				velY = velY*(-1);
				ball.x = ball.x + velX;
				ball.y = ball.y + velY;
			}
			
			repaint();
			this.getToolkit().sync();
			
			try{
				
				Thread.sleep(8);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
		
	}
	/**
	 * method paint is for paint balls
	 * ball is yellow
	 */
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.draw(box);
		
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball);
	}

}

package pilasuk.warisara.lab8;

/**
 * this program names WrapAroundBall
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.*;
import javax.swing.*;

public class WrapAroundBall extends JPanel implements Runnable{

	/**
	 * this class extends JPanel
	 * this class implements Runnable
	 */
	private static final long serialVersionUID = 3741759472837563171L;
	
	MovingBall ball, ball2, ball3, ball4, ball5;
	Thread running;
	int velX = 1;
	int velY = 1;
	
	/**
	 * this method is for set background color is blue
	 * balls position
	 */
	public WrapAroundBall() {
		setBackground(Color.BLUE);
		ball = new MovingBall((SimpleGameWindow.WIDTH)/2-20, 0, 20, 20, 20);
		ball2 = new MovingBall(0,(SimpleGameWindow.HEIGHT)/2-20, 20, 20, 20);
		ball3 = new MovingBall(0, 0, 20, 20, 20);
		ball4 = new MovingBall((SimpleGameWindow.WIDTH)/2-20,(SimpleGameWindow.HEIGHT)-20, 20, 20, 20);
		ball5 = new MovingBall(SimpleGameWindow.WIDTH-20, (SimpleGameWindow.HEIGHT)/2-20, 20, 20, 20);
		
		running = new Thread(this);
		running.start();
	}
	
	/**
	 * Override method run for check if else 
	 */
	@Override
	public void run() {
		
		while(true) {
			
			ball.y = ball.y + velY;
			
			ball2.x = ball2.x + velX;
			
			ball3.x = ball3.x + velX;
			ball3.y = ball3.y + velY;
			
			ball4.y = ball4.y - velY;
			
			ball5.x = ball5.x - velX;

			
			if ((ball.y > SimpleGameWindow.HEIGHT) || (ball.y > SimpleGameWindow.WIDTH)) {
				ball.x = 305;
				ball.y = 0;
			}
			if ((ball2.x > SimpleGameWindow.HEIGHT) || (ball2.x > SimpleGameWindow.WIDTH)) {
				ball2.x = 0;
				ball2.y = 230;
			}
			if ((ball3.x > SimpleGameWindow.HEIGHT) || (ball3.y > SimpleGameWindow.WIDTH)) {
				ball3.x = 0;
				ball3.y = 0;
			}
			if ((ball4.y < (SimpleGameWindow.HEIGHT-500)) || (ball4.y > SimpleGameWindow.WIDTH)) {
				ball4.x = 200;
				ball4.y = SimpleGameWindow.HEIGHT;
			}
			if ((ball5.x < (SimpleGameWindow.HEIGHT-500)) || (ball5.x > SimpleGameWindow.WIDTH)) {
				ball5.x = SimpleGameWindow.WIDTH;
				ball5.y = 300;
			}
					
			repaint();
			this.getToolkit().sync();
			
			try{
				
				Thread.sleep(10);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
		
	}
	
	/**
	 * method paint is for set balls color and draw balls
	 * ball is yellow
	 * ball2 is red
	 * ball3 is green
	 * ball4 is black
	 * ball5 is white
	 */
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball);
		
		g2d.setColor(Color.RED);
		g2d.fill(ball2);
		
		g2d.setColor(Color.GREEN);
		g2d.fill(ball3);
		
		g2d.setColor(Color.BLACK);
		g2d.fill(ball4);
		
		g2d.setColor(Color.WHITE);
		g2d.fill(ball5);
		
	}
}

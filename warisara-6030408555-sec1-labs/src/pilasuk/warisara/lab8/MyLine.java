package pilasuk.warisara.lab8;

/**
 * this program names MyLine
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.geom.Line2D;

public class MyLine extends Line2D.Double {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyLine(int x, int y, int y1, int y2 ) {
		super(x, y, y1, y2);
	}

}

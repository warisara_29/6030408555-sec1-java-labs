package pilasuk.warisara.lab8;

/**
 * this program names TestBouncyBall
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import javax.swing.SwingUtilities;

public class TestBouncyBall extends SimpleGameWindow{
	public TestBouncyBall(String string) {
		super(string);
		
	}
	private static final long serialVersionUID = 1L;
	
	protected void addComponents() {
		BouncyBall panel = new BouncyBall();
		setContentPane(panel);
	}
	
	public static void createAndShowGUI() {
		TestBouncyBall window = new TestBouncyBall("Test Bouncy Ball");
		window.setFrameFeatures();
		window.addComponents();
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}


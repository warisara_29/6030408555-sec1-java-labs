package pilasuk.warisara.lab8;

/**
 * this program names PongPaddle 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.geom.Rectangle2D;

public class PongPaddle extends Rectangle2D.Double {

	/**
	 * this class is extends Rectangle2D class
	 * this class is for set pong paddle
	 */
	private static final long serialVersionUID = 1L;
	
	public PongPaddle(int x, int y, int w, int h) {
		super(x, y, w, h);
		
	}
	
}

package pilasuk.warisara.lab8;

/**
 * this program names MovingBall 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

public class MovingBall extends Ball{
	/**
	 * this class is for get and set velX and velY
	 * velX is velocity x
	 * velY is velocity y
	 */
	private static final long serialVersionUID = 1L;
	private int velX;
	private int velY;
	
	public double getVelX() {
		return velX;
	}
	public void setVelX(int velX) {
		this.velX = velX;
	}
	public double getVelY() {
		return velY;
	}
	public void setVelY(int velY) {
		this.velY = velY;
	}
	
	public MovingBall(int x, int y, int r, int velX, int velY) {
		super(x, y, r);
	}
}

package pilasuk.warisara.lab8;

/**
 * this program names TestWrapAroundBall
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import javax.swing.SwingUtilities;

public class TestWrapAroundBall extends SimpleGameWindow {

	public TestWrapAroundBall(String string) {
		super(string);
		
	}
	private static final long serialVersionUID = 1L;
	
	protected void addComponents() {
		WrapAroundBall panel = new WrapAroundBall();
		setContentPane(panel);
	}
	
	public static void createAndShowGUI() {
		TestWrapAroundBall window = new TestWrapAroundBall("Test Wrap Around Ball");
		window.setFrameFeatures();
		window.addComponents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

package pilasuk.warisara.lab8;

/**
 * this program names PongGameGUI 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

public class PongGameGUI extends SimpleGameWindow {

	/**
	 * this class extends SimpleGameWindow class
	 */
	private static final long serialVersionUID = 1L;
	
	public PongGameGUI(String string) {
		super(string);
	}
	
	public static void main(String[] args) {
		PongPanel myPanel = new PongPanel();
		SimpleGameWindow sgw = new SimpleGameWindow("Pong Game Window");
		sgw.setFrameFeatures();
		sgw.add(myPanel);
	}

}

package pilasuk.warisara.lab8;

/**
 * this program names PongPanel
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

public class PongPanel extends JPanel {

	/**
	 * this class extends JPanel
	 */
	private static final long serialVersionUID = 1L;
	
	private Ball ball;
	private PongPaddle paddleL, paddleR;
	private int scorePlayer1 = 0 , scorePlayer2 = 0;
	private int r = 20;
	
	/**
	 * Override method Dimension
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(SimpleGameWindow.WIDTH,SimpleGameWindow.HEIGHT);
		
	}
	
	/**
	 * method PongPanel is for set background color is black
	 * paddleL is for set position pong paddle left
	 * paddleR is for set position pong paddle right
	 * ball is for set the ball position in the middle
	 */
	protected PongPanel() {
		setBackground(Color.BLACK);
		
		paddleL = new PongPaddle(0, 250 - 40,15, 80);
		paddleR = new PongPaddle(SimpleGameWindow.WIDTH - 15, 250-40, 15, 80);
		ball = new Ball((SimpleGameWindow.WIDTH)/2 - r, (SimpleGameWindow.HEIGHT)/2 - r,r);
	}
	
	/**
	 * Override method paintComponent is for draw and set font
	 * set lines position
	 * set lines color is white
	 * draw lines, paddleL, paddleR, ball
	 * set font score is Serif, font score size = 48
	 */
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		
		Line2D leftline = new Line2D.Double(15, 0, 15, SimpleGameWindow.HEIGHT);
		Line2D middleline = new Line2D.Double(SimpleGameWindow.WIDTH/2, 0, SimpleGameWindow.WIDTH/2, SimpleGameWindow.HEIGHT);
		Line2D rightline = new Line2D.Double(SimpleGameWindow.WIDTH-15, 0, SimpleGameWindow.WIDTH-15, SimpleGameWindow.HEIGHT);
		
		g2d.setColor(Color.WHITE);
		g2d.draw(leftline);
		g2d.draw(rightline);
		g2d.draw(middleline);
		
		
		g2d.draw(paddleL);
	
		g2d.draw(paddleR);
		
		
		g2d.fill(ball);
		g2d.fill(paddleL);
		g2d.fill(paddleR);
		
		Font score = new Font("Serif", Font.BOLD, 48);
		g2d.setFont(score);
		g2d.drawString(Integer.toString(scorePlayer1), SimpleGameWindow.WIDTH/4, 100);
		g2d.drawString(Integer.toString(scorePlayer2), SimpleGameWindow.WIDTH*3/4, 100);
	}
	
}

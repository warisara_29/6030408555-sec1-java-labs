package pilasuk.warisara.lab8;

/**
 * this program names SimpleGameWindow
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   25/03/2018
 **/

import java.awt.*;
import javax.swing.*;

public class SimpleGameWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	
	static final int WIDTH = 650;
	static final int HEIGHT = 500;
	
	public SimpleGameWindow(String string) {
		super (string);
	}

	public static void createAndShowGUI() {
		SimpleGameWindow sgw = new SimpleGameWindow("Simple Game Window");
		sgw.setFrameFeatures();
	}
	
	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2 - WIDTH/2;
		int y = (dim.height - h)/2 - HEIGHT/2;
		setSize(WIDTH, HEIGHT);
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
		 

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
}

package pilasuk.warisara.lab11;

/**
 * this program names AthleteFromV7
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   30/04/2018
 **/

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import pilasuk.warisara.lab9.AthleteFormV6;


public class AthleteFormV7 extends AthleteFormV6 {

	public AthleteFormV7(String title) {
		super(title);
	}
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void addComponents() {
		super.addComponents();
		/*
		 * Implement accelerators and mnemonic keys to menu File, Config and menu item
		 */
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuItem_New.setMnemonic(KeyEvent.VK_N);
		menuItem_Open.setMnemonic(KeyEvent.VK_O);
		menuItem_Save.setMnemonic(KeyEvent.VK_S);
		menuItem_Exit.setMnemonic(KeyEvent.VK_Q);
		
		menuItem_New.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		menuItem_Open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		menuItem_Save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuItem_Exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
	}	
	
	@Override
	protected void addListeners() {
		super.addListeners();
		
		menuItem_Open.addActionListener(this);
		menuItem_Save.addActionListener(this);
		menuItem_Exit.addActionListener(this);
		custumcolor.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		JFileChooser fileChooser = new JFileChooser(); //create file chooser
		if (event.getSource() == menuItem_Open) { // check if open menu is selected program will open file 
			int returnValue = fileChooser.showOpenDialog(this);
			if(returnValue == JFileChooser.APPROVE_OPTION) {
				File openFile = fileChooser.getSelectedFile();
				String message = "Openning: " + openFile.getName() + ". \n";
				JOptionPane.showMessageDialog(this, message);
			} else {
				JOptionPane.showMessageDialog(this, "Open command cancelled by user.");
			}
		} else if (event.getSource() == menuItem_Save) { // check if save menu is selected program will save file
			int returnValue = fileChooser.showSaveDialog(this);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File saveFile = fileChooser.getSelectedFile();
				String message = "Saving: " + saveFile.getName() + ". \n";
				JOptionPane.showMessageDialog(this, message);
			} else {
				JOptionPane.showMessageDialog(this, "Open command cancelled by user.");
			}
		} else if (event.getSource() == menuItem_Exit) { // check if exit menu is selected program will close
			System.exit(0);
		} else if (event.getSource() == custumcolor) { // check if custum color menu is selected program will shows color dialog
			Color color = JColorChooser.showDialog(null, "Color Chooser", Color.WHITE);
			text_area.setBackground(color);
		}
	}	   
	
	public static void createAndShowGUI(){
		AthleteFormV7 AthleteForm7 = new AthleteFormV7("Athlete Form V7");
		AthleteForm7.addComponents();
		AthleteForm7.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

package pilasuk.warisara.lab11;

/**
 * this program names AthleteFromV8
 * When the user fills the information of an athlete in the form and then press button OK, 
 * the program will display the information in the dialog
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   30/04/2018
 **/

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

import pilasuk.warisara.lab5.Athlete;
import pilasuk.warisara.lab5.Gender;

public class AthleteFormV8 extends AthleteFormV7 {

	public AthleteFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	protected ArrayList<Athlete> athletes = new ArrayList<Athlete>();
	protected Athlete arrayAthlete;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void addComponents() {
		super.addComponents();
	}
	
	public void ActionperFormed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == ok_Button) {
			addAthlete();
		}
	}
	@Override
	protected void addListeners() {
		super.addListeners();
	}
	
	private void addAthlete() {
		double weight = Double.parseDouble(weightField.getText());
		double height = Double.parseDouble(heightField.getText());
		
		if (radioButton1.isSelected()) {
			gender = Gender.MALE;
		} else if (radioButton2.isSelected()) {
			gender = Gender.FEMALE;
		}
		
		arrayAthlete = new Athlete(nameField.getText(), weight, height, gender, nationalityField.getText(),
				birthdateField.getText());
		athletes.add(arrayAthlete);
		System.out.println(athletes);
	}

	public static void createAndShowGUI(){
		AthleteFormV8 AthleteForm8 = new AthleteFormV8("Athlete Form V8");
		AthleteForm8.addComponents();
		AthleteForm8.setFrameFeatures();
		//AthleteForm8.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

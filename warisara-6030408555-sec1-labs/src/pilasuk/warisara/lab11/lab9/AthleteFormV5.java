package pilasuk.warisara.lab9;

/**
 * this program names AthleteFormV5
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   1/04/2018
 **/

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;

/**
 * class extends AthleteFromV4 and implements ActionListener
 */

public class AthleteFormV5 extends AthleteFormV4 implements ActionListener{

	public AthleteFormV5(String title) {
		super(title);
	}
	private static final long serialVersionUID = 1L;
	
	protected JMenuItem blue, green, red, custumcolor, sixteen, twenty, twentyfour, custumsize;
	
	/**
	 * @Override method actionPerformed is for check when user click buttons
	 */
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == blue) {
			handlerColor(Color.BLUE);
		}
		else if (src == green) {
			handlerColor(Color.GREEN);
		}
		else if (src == red) {
			handlerColor(Color.RED);
		}
		else if (src == sixteen) {
			int newsize = Integer.valueOf(sixteen.getText());
			handlerSize(newsize);
		}
		else if (src == twenty) {
			int newsize = Integer.valueOf(twenty.getText());
			handlerSize(newsize);
		}
		else if (src == twentyfour) {
			int newsize = Integer.valueOf(twentyfour.getText());
			handlerSize(newsize);
		}
	}
	/**
	 * method handlerColor is for check when user click color(blue, green,red)
	 * it will change text color as user click some color
	 */
	private void handlerColor(Color color) {
		nameField.setForeground(color);
		birthdateField.setForeground(color);
		weightField.setForeground(color);
		heightField.setForeground(color);
		nationalityField.setForeground(color);
		text_area.setForeground(color);
	}
	
	/**
	 * method handlerSize is for check when user click size(16, 20,24)
	 * it will change text size as user click some size
	 */
	private void handlerSize(int size) {
		Font newfont = new Font(Font.SANS_SERIF, Font.BOLD, size);
		nameField.setFont(newfont);
		birthdateField.setFont(newfont);
		weightField.setFont(newfont);
		heightField.setFont(newfont);
		nationalityField.setFont(newfont);
		text_area.setFont(newfont);
	}
	/*
	 * method addmenuConfig is for add menu item color and menu item size
	 */
	protected void addmenuConfig() {
		super.addMenuBar();
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		custumcolor = new JMenuItem("Custom...");

		sixteen = new JMenuItem("16");
		twenty = new JMenuItem("20");
		twentyfour = new JMenuItem("24");
		custumsize = new JMenuItem("Custom...");
	}
	/*
	 * method handlerColor is for listen when user click some button
	 */
	protected void addListeners() {
		super.addListeners();
		addmenuConfig();
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		
		sixteen.addActionListener(this);
		twenty.addActionListener(this);
		twentyfour.addActionListener(this);

	}
	protected void addComponents() {
		super.addComponents();
		menuConfig.removeAll();
		menuItem_Color.add(blue);
		menuItem_Color.add(green);
		menuItem_Color.add(red);
		menuItem_Color.add(custumcolor);
		
		menuItem_Size.add(sixteen);
		menuItem_Size.add(twenty);
		menuItem_Size.add(twentyfour);
		menuItem_Size.add(custumsize);
		menuConfig.add(menuItem_Color);
		menuConfig.add(menuItem_Size);
	}
	
    public static void createAndShowGUI(){
    	AthleteFormV5 AthleteForm5 = new AthleteFormV5("Athlete From V5");
    	AthleteForm5.addComponents();
    	AthleteForm5.setFrameFeatures();
    }
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			createAndShowGUI();
    		}
    	});
    }
}

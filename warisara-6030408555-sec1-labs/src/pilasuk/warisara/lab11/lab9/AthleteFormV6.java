package pilasuk.warisara.lab9;

/**
 * this program names AthleteFormV�
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   1/04/2018
 **/

import java.awt.*;
import java.net.URL;
import javax.swing.*;

/**
 * class extends AthleteFromV5
 */

public class AthleteFormV6 extends AthleteFormV5 {

	public AthleteFormV6(String title) {
		super(title);
	}
	
	private static final long serialVersionUID = 1L;
	protected JPanel panel6, panelpic;
	protected JLabel picture;
	protected URL boxer_file, open_file, save_file , exit_file;
	protected ImageIcon boxer_icon, open_icon, save_icon, exit_icon;
	
	/*
	 * method addComponents is for create new 2 panel
	 * panelpic is panel for add picture and panel6 is for add panelpic and all information
	 */
	@Override
	protected void addComponents() {
		super.addComponents();
		panel6 = new JPanel(new BorderLayout());
		panelpic = new JPanel(new BorderLayout());
		boxer_file = this.getClass().getResource("mana.jpg"); // for get images url
		open_file = this.getClass().getResource("openIcon.png");
		save_file = this.getClass().getResource("saveIcon.png");
		exit_file = this.getClass().getResource("quitIcon.png");
		
		boxer_icon = new ImageIcon(boxer_file); //add images in icon
		open_icon = new ImageIcon(open_file);
		save_icon = new ImageIcon(save_file);
		exit_icon = new ImageIcon(exit_file);
		
		menuItem_Open.setIcon(open_icon); // add icon in menu item
		menuItem_Save.setIcon(save_icon);
		menuItem_Exit.setIcon(exit_icon);
		
		picture = new JLabel(boxer_icon);

		int x = 25;
		panel6.setBorder(BorderFactory.createEmptyBorder(x,x,x,x));
		panelpic.add(picture);
		
		panel6.add(panel_Panel, BorderLayout.CENTER);
		panel6.add(panelpic, BorderLayout.NORTH);
		panel6.add(panel, BorderLayout.SOUTH);
		this.setContentPane(panel6);
	
	}
	
	protected void addListeners() {
		super.addListeners();
	}

	public static void createAndShowGUI(){
		AthleteFormV6 AthleteForm6 = new AthleteFormV6("Athlete Form V6");
		AthleteForm6.addComponents();
		AthleteForm6.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

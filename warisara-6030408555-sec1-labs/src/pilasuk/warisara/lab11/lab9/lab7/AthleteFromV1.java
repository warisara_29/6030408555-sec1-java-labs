package pilasuk.warisara.lab9.lab7;

/**
 * this program names AthleteFromV1 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   12/03/2018
 **/

import java.awt.*;

import javax.swing.*;

public class AthleteFromV1 extends MySimpleWindow {

	public AthleteFromV1(String title) {
		super(title);
	}

	/**
	 * MySimpleWindow extends from class MySimpleWindow
	 * MySimpleWindow has 1 constructor and 4 methods
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private int textLenght = 15;
	
	protected JTextField nameField;
	protected JTextField birthdateField;
	protected JTextField weightField;
	protected JTextField heightField;
	protected JTextField nationalityField;
	protected JPanel textAndFiedPane;
	
	protected void addTextLabelAndField(JPanel panel, JLabel label, JTextField textField) {
		textAndFiedPane = new JPanel();
		textAndFiedPane.setLayout(new GridLayout(1, 2));
		textAndFiedPane.add(label);
		textAndFiedPane.add(textField);
		textAndFiedPane.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0)); //createEmptyBorder(int top, int left, int bottom, int right)
		panel.add(textAndFiedPane);
	}
	
	/**
	 * method addComponents is for set box layout, set label, set text field
	 * set Border layout = north
	 */
	protected void addComponents() {
		super.addComponents();
		panel_Panel.setLayout(new BoxLayout(panel_Panel, BoxLayout.Y_AXIS));
		
		JLabel nameLabel = new JLabel("Name:");
		JLabel birthdateLabel = new JLabel("Birthdate:");
		JLabel weightLabel = new JLabel("Weight (kg.):");
		JLabel heightLabel = new JLabel("Height (metre):");
		JLabel nationalityLabel = new JLabel("Nationality:");
		
		nameField = new JTextField(textLenght);
		birthdateField = new JTextField(textLenght);
		weightField = new JTextField(textLenght);
		heightField = new JTextField(textLenght);
		nationalityField = new JTextField(textLenght);
		
		addTextLabelAndField(panel_Panel, nameLabel, nameField);
		addTextLabelAndField(panel_Panel, birthdateLabel, birthdateField);
		birthdateField.setToolTipText("ex. 22.02.2000");
		addTextLabelAndField(panel_Panel, weightLabel, weightField);
		addTextLabelAndField(panel_Panel, heightLabel, heightField);
		addTextLabelAndField(panel_Panel, nationalityLabel, nationalityField);
		add(panel_Panel, BorderLayout.NORTH);
	}
	/**
	 * method createAndShowGUI is for create and show method addCompoents and method setFrameFeatures
	 * set AthleteFromV1 title = Athlete From V1
	 */
	public static void createAndShowGUI() {
		AthleteFromV1 athleteFrom1 = new AthleteFromV1("Athlete From V1");
        athleteFrom1.addComponents();
        athleteFrom1.setFrameFeatures();
	}
	
	/**
	 * @param args
	 * for run method createAndShowGUI
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
	           createAndShowGUI();
	        }
		});
	}
}

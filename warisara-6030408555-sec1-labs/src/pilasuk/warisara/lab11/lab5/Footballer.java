package pilasuk.warisara.lab5;

/**
 * this program is a subclass of Athlete and has private instance variables sport (that is set to "American Football"), position and team
 * 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/

public class Footballer extends Athlete {
	
	/**
	 * Add private instance variables 
	 * static String variable called sport that is set to "American Football".
	 * private double variable called position.
	 * private int variable called team.
	 * @param Sport this is the first parameter for set sport to American Football
	 * @param position this is the second parameter for get and set position
	 * @param team this is the third parameter for get and set team
	 * @return Boxing, position(String), team(String)
	 */ 
	
	private static String Sport = "American Football";
	private String position;
	private String team;

	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String position, String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	}
	
	/**
	 * method get is for get football player's informations from the program
	 * method set is for set football player's informations from the program
	 */

	
	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		Footballer.Sport = sport;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	public void play() {
		System.out.println(super.getName() + " throws a touchdown.");
	}
	
	public void move() {
		System.out.println(super.getName() + " moves down the football field.");
	}
	@Override
	public void playSport() {
		System.out.println(super.getName() + " is good at " + getSport());
	}
	
	/**
	 * this is override method for set the form of the program to show football player's  informations
	 * @return getName, getWeight, getHeight, getGender, getNationality, getBirthdate, getSport, getPosition, getTeam
	 */
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getPosition() + ", " + getTeam() ;
	}
	
}

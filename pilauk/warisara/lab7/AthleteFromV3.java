package pilasuk.warisara.lab7;

/**
 * this program names AthleteFromV3
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   12/03/2018
 **/


import java.awt.*;

import javax.swing.*;

public class AthleteFromV3 extends AthleteFromV2 {

	public AthleteFromV3(String title) {
		super(title);
	}

	/**
	 * MySimpleWindow extends from class MySimpleWindow
	 * MySimpleWindow has 1 constructor and 4 methods
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * method addMenuBar is for set menu bar and menu item
	 * menu bar are File and Config
	 * menu bar File are New, Open, Save and Exit
	 * menu bar Config are Color and Size
	 */
	protected void addMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenu menuFile = new JMenu("File");
		JMenu menuConfig = new JMenu("Config");
		menuBar.add(menuFile);
		menuBar.add(menuConfig);
		
		JMenuItem menuItem_New = new JMenuItem("New");
		JMenuItem menuItem_Open = new JMenuItem("Open");
		JMenuItem menuItem_Save = new JMenuItem("Save");
		JMenuItem menuItem_Exit = new JMenuItem("Exit");
		menuFile.add(menuItem_New);
		menuFile.add(menuItem_Open);
		menuFile.add(menuItem_Save);
		menuFile.add(menuItem_Exit);
		
		JMenuItem menuItem_Color = new JMenuItem("Color");
		JMenuItem menuITem_Size = new JMenuItem("Size");
		menuConfig.add(menuItem_Color);
		menuConfig.add(menuITem_Size);
		
		this.setJMenuBar(menuBar);
	}
	/**
	 * method addComponents is for set and show typeLabel and athleteType
	 * JComBox is for show athlete type (Badminton Player, Boxer and Footballer)
	 * set Border layout = north
	 */
	protected void addComponents() {
		super.addComponents();
		JPanel typeLabel = new JPanel();
		JPanel athleteType = new JPanel();

		JLabel typeShow = new JLabel("Type: ");
		typeLabel.setLayout(new GridLayout(1, 2));
		typeLabel.add(typeShow);
		typeLabel.add(athleteType);
		panel_Panel.add(typeLabel);
		
		JComboBox<String> athlete = new JComboBox<String>();
		athlete.addItem("Badminton Player");
		athlete.addItem("Boxer");
		athlete.addItem("Footballer");
		typeLabel.add(athlete);
		add(panel_Panel, BorderLayout.NORTH);
		
	}
	
	/**
	 * method createAndShowGUI is for create and show method addCompoents, method setFrameFeatures
	 * and method setFrameFeatures
	 * set AthleteFromV3 title = Athlete From V3 
	 */
    public static void createAndShowGUI(){
    	AthleteFromV3 AthleteFrom3 = new AthleteFromV3("Athlete From V3");
    	AthleteFrom3.addMenuBar();
    	AthleteFrom3.addComponents();
    	AthleteFrom3.setFrameFeatures();
    }
	/**
	 * @param args
	 * for run method createAndShowGUI
	 */
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			createAndShowGUI();
    		}
    	});
    }
}

package pilasuk.warisara.lab5;
/**
 * The Athlete program implements an application that record athlete personal information : name, weight, height, gender, nationality, birthdate
 * 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 * 
 **/

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Athlete {
	
	/**
	   * The class has private instance variables to record 
	   * the following information: name, weight, height, gender, nationality, birthdate
	   * @param name this is the first parameter to get and set name 
	   * @param weight this is the second parameter to get and set weight
	   * @param height this is the third parameter to get and set height
	   * @param gender this is the fourth parameter to enum gender(male and female)
	   * @param nationality this is the fifth parameter to get and set nationality
	   * @param birthdate this is the sixth parameter to get and set birthdate
	   * @return name(string), weight(double), height(double), gender(enum Gender), nationality(string), birthdate(string)
	   **/

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate , formatter);
	}
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); //for set date(day/month/year) by import java.lang LocalDate
	
	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	
	//method get is for get athlete information from program
	//method set is for set athlete information from program
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getWeight() {
		return weight;		
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	/**
	 * this is the method palySport for description if athlete good at their sport
	 * @param playSport this is public void method to get name and description
	 * @return nothing
	 */
	public void playSport() {
		System.out.println(name + " is good at sport");
	}
	/**
	 * this is override method for set the from of the program to show athlete informations
	 * @return name, weight, height, gender, nationality, birthdate
	 */
	@Override
	public String toString() {
		return "Athlete [" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", " + birthdate + " ]";
	}	
}

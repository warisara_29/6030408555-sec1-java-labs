package pilasuk.warisara.lab5;
/**
 *this program is a subclass of Athlete and has private instance variables sport (that is set to "Badminton"), racketLength, worldRanking
 * 
 * @author  Warisara Pilasuk
 * @version JavaSE_1.8
 * @since   10/02/2018
 **/

import java.time.temporal.ChronoUnit;

public class BadmintonPlayer extends Athlete {
	
/**
 * Add private instance variables 
 * static String variable called sport that is set to "Badminton".
 * private double variable called racketLength.
 * private int variable called worldRanking.
 * @param Sport this is the first parameter for set sport to Badminton
 * @param racketLenght this is the second parameter for get and set racket lenght
 * @param worldRanking this is the third parameter for get and set world ranking
 * @return Badminton, racketLenght(double), worldRanking(int)
 */
	private static String Sport = "Badminton";
	private double racketLenght;
	private int worldRanking;
	private String equipment = "Shuttlecock";

	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality, String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLenght = racketLength;
		this.worldRanking = worldRanking;
	}
	//method get is for get badminton player's informations from the program
	//method set is for set badminton player's informations from the program
	public double getRacketLenght() {
		return racketLenght;
	}

	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	public void setRacketLenght(double racketLenght) {
		this.racketLenght = racketLenght;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	

	public static String getSport() {
		return Sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.Sport = sport;
	}
	
	public void play() {
		System.out.println(super.getName() + " hits a shuttlecock.");
	}
	
	public void move() {
		System.out.println(super.getName() + " moves around badminton court.");
	}
	@Override
	public void playSport() {
		System.out.println(super.getName() + " is good at " + Sport);
	}
	/**
	 * this is override method for set the form of the program to show badminton player's  informations
	 * @return getName, getWeight, getHeight, getGender, getNationality, getBirthdate, getSport, getRacketLenght, getWorldranking
	 */
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender() + ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getRacketLenght() + "cm, rank: " + getWorldRanking() + " equipment:" + equipment;
	}
	
	
	/**
	 * the method compareAge  that accepts another athlete then compare the age of an instance athlete 
	 * to that of the other athlete input as parameter
	 * @param localDate is for compare age between athlete
	 * @boolean year is for compare who is older
	 */
	public void compareAge(Object localDate) {
		int year = (int) ChronoUnit.YEARS.between(super.getBirthdate(), (((Athlete) localDate).getBirthdate()));
		if (year < 0) {
			System.out.println(((Athlete) localDate).getName() + " is " + Math.abs(year) + " years older than " + super.getName());
		} else if (year == 0) {
			System.out.println(((Athlete) localDate).getName() + " had year old equal to " + super.getName());
		} else {
			System.out.println(super.getName() + " is " + year + " years older than " + ((Athlete) localDate).getName());
		}
	}
}